def reverser(&proc)
  sentence = proc.call
  words = sentence.split(" ")
  words.map { |word| word.reverse }.join(" ")
end

def adder(increment = 1, &proc)
  proc.call + increment
end

def repeater(repeats = 1, &proc)
  repeats.times { proc.call }
end
