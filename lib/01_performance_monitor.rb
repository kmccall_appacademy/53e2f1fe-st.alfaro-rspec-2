def measure(n_times = 1, &proc)
  start_time = Time.now
  n_times.times { proc.call }
  (Time.now - start_time) / n_times
end

def measure_yield
  start_time = Time.now
  n_times.times { yield }
  (Time.now - start_time) / n_times
end
